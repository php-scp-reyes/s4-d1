<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S4: Access Modifiers and Encapsulation</title>
</head>
<body>
	<h1>Access Modifiers</h1>

	<p><?= $building->setName("Manulife Building"); ?></p>

	<p><?= $building->getName(); ?></p>

	<p><?= $condominium->setName("Trump Tower"); ?></p>

	<p><?= $condominium->getName(); ?></p>
</body>
</html>
